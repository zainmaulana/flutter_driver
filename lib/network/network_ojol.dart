import 'dart:convert';

import 'package:vmsdriver/models/model_auth.dart';
import 'package:vmsdriver/models/model_driveracceptorder.dart';
import 'package:vmsdriver/models/model_driverchat.dart';
import 'package:vmsdriver/models/model_driverchatinput.dart';
import 'package:vmsdriver/models/model_drivercompleteorder.dart';
import 'package:vmsdriver/models/model_drivergetdetail.dart';
import 'package:vmsdriver/models/model_driverhistory.dart';
import 'package:vmsdriver/models/model_driverinsertlokasi.dart';
import 'package:vmsdriver/models/model_drivernotification.dart';
import 'package:vmsdriver/models/model_driverstartorder.dart';
import 'package:vmsdriver/models/model_getprofile.dart';
import 'package:http/http.dart' as http;

class NetworkOjol {
  static String _host = "vms.hutamakarya.com";

  // Future<ModelAuth> registerDriver(
  //     String email, String password, String name, String phone) async {
  //   final url = Uri.http(_host, "serverojol/api/daftar/23");
  //   final response = await http.post(url, body: {
  //     "nama": name,
  //     "email": email,
  //     "password": password,
  //     "phone": phone
  //   });

  //   if (response.statusCode == 200) {
  //     ModelAuth responseRegis = ModelAuth.fromJson(jsonDecode(response.body));
  //     return responseRegis;
  //   } else {
  //     return null;
  //   }
  // }

  Future<ModelAuth> loginDriver(
      String email, String password, String device) async {
    final url = Uri.https(_host, "api/login_driver");
    final response = await http.post(url, body: {
      "r_email": email,
      "r_password": password,
      "r_device": device,
    });
    if (response.statusCode == 200) {
      ModelAuth responseLogin = ModelAuth.fromJson(jsonDecode(response.body));
      return responseLogin;
    } else {
      return null;
    }
  }

  Future<ModelHistoryDriver> registerFcm(
    String iduser,
    String fcm,
  ) async {
    final uri = Uri.https(_host, "api/register_fcm");
    final response = await http.post(uri, body: {
      "r_id_user": iduser,
      "r_fcm_token": fcm,
    });
    if (response.statusCode == 200) {
      ModelHistoryDriver responseData =
          ModelHistoryDriver.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelGetProfile> getProfileUser(
    String id,
  ) async {
    final url = Uri.https(_host, "api/get_data_driver");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelGetProfile responseData =
          ModelGetProfile.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelGetDetailOrder> getDetailOrderDriver(
    String id,
  ) async {
    final url = Uri.https(_host, "api/detail_order_driver");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelGetDetailOrder responseData =
          ModelGetDetailOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelHistoryDriver> getHistoryOrder(
    String id,
    String status,
  ) async {
    final url = Uri.https(_host, "api/list_order_driver");
    final response = await http.post(url, body: {
      "r_id_user": id,
      "r_id_order_status": status,
    });
    if (response.statusCode == 200) {
      ModelHistoryDriver responseData =
          ModelHistoryDriver.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelAcceptOrder> acceptOrder(
    String idOrder,
    String id,
  ) async {
    final uri = Uri.https(_host, "api/accept_order");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelAcceptOrder responseData =
          ModelAcceptOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelStartOrder> startOrder(
    String idOrder,
  ) async {
    final uri = Uri.https(_host, "api/start_order");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
    });
    if (response.statusCode == 200) {
      ModelStartOrder responseData =
          ModelStartOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  //API New Complete Order
  Future<ModelCompleteOrder> completeOrderNew(
    String idOrder,
    String id,
  ) async {
    final uri = Uri.https(_host, "api/complete_order_new");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelCompleteOrder responseData =
          ModelCompleteOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelCompleteOrder> completeOrder(
    String idOrder,
    String id,
  ) async {
    final uri = Uri.https(_host, "api/complete_order");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelCompleteOrder responseData =
          ModelCompleteOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelInsertLokasi> insertLokasi(
    String idOrder,
    String id,
    String lastLat,
    String lastLng,
    String lastLoc,
  ) async {
    final uri = Uri.https(_host, "api/insert_lokasi");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
      "r_id_user": id,
      "r_last_latitude": lastLat,
      "r_last_longitude": lastLng,
      "r_last_location": lastLoc,
    });
    if (response.statusCode == 200) {
      ModelInsertLokasi responseData =
          ModelInsertLokasi.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelNotificationDriver> getNotifUser(
    String id,
  ) async {
    final url = Uri.https(_host, "api/list_notif");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelNotificationDriver responseData =
          ModelNotificationDriver.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelNotificationDriver> deleteNotifUser(
    String idNotif,
  ) async {
    final url = Uri.https(_host, "api/delete_notif");
    final response = await http.post(url, body: {
      "r_id_notif": idNotif,
    });
    if (response.statusCode == 200) {
      ModelNotificationDriver responseData =
          ModelNotificationDriver.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelDriverChatInput> inputChat(
    String idOrder,
    String id,
    String chatText,
  ) async {
    final url = Uri.https(_host, "api/chatdriver");
    final response = await http.post(url, body: {
      "r_id_order": idOrder,
      "r_id_user": id,
      "r_chat": chatText,
    });
    if (response.statusCode == 200) {
      ModelDriverChatInput responseData =
          ModelDriverChatInput.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelChatEach> getEachChat(
    String id,
  ) async {
    final url = Uri.https(_host, "api/each_chat");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelChatEach responseData =
          ModelChatEach.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelChatEach> getChats(
    String idOrder,
  ) async {
    final url = Uri.https(_host, "api/chat_room");
    final response = await http.post(url, body: {
      "r_id_order": idOrder,
    });
    if (response.statusCode == 200) {
      ModelChatEach responseData =
          ModelChatEach.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }
}
