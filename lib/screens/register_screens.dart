import 'package:vmsdriver/network/network_ojol.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class RegisterScreen extends StatefulWidget {
  static String tag = "register-page";
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _nohp = TextEditingController();
  final FocusNode passwordNode = FocusNode();
  final FocusNode nameNode = FocusNode();
  final FocusNode noHpNode = FocusNode();
  bool _obscoreText = true;
  NetworkOjol networkOjol = NetworkOjol();
  final _key = GlobalKey<ScaffoldState>();

  void _toogle() {
    setState(() {
      _obscoreText = !_obscoreText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("gambar/background.png"), fit: BoxFit.fill)),
      child: Scaffold(
        backgroundColor: Colors.black,
        key: _key,
        appBar: AppBar(
          title: Text("Register"),
        ),
        body: Column(
          children: [
            myTextField(
              _email,
              null,
              TextInputType.emailAddress,
              "input your email",
              "email",
              Icon(Icons.person),
              null,
              false,
              passwordNode,
            ),
            myTextField(
              _password,
              passwordNode,
              TextInputType.visiblePassword,
              "input your password",
              "password",
              Icon(Icons.lock),
              IconButton(
                  icon: Icon(
                      _obscoreText ? Icons.visibility : Icons.visibility_off),
                  onPressed: _toogle),
              _obscoreText,
              nameNode,
            ),
            myTextField(
              _name,
              nameNode,
              TextInputType.text,
              "input your name",
              "name",
              Icon(Icons.person),
              null,
              false,
              noHpNode,
            ),
            myTextField(
              _nohp,
              noHpNode,
              TextInputType.phone,
              "input your phone",
              "phone",
              Icon(Icons.call),
              null,
              false,
              null,
            ),
            Container(
              width: double.infinity,
              height: 40,
              margin: EdgeInsets.only(top: 10),
              child: RaisedButton(
                onPressed: () {
                  setState(() {
                    if (_email.text.isEmpty ||
                        _password.text.isEmpty ||
                        _name.text.isEmpty ||
                        _nohp.text.isEmpty) {
                      Toast.show("inputan tidak boleh kosong", context);
                    } else {
                      // prosesRegister();
                    }
                  });
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.blue[400],
                child: Text(
                  "Register",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // void prosesRegister() {
  //   networkOjol
  //       .registerDriver(_email.text, _password.text, _name.text, _nohp.text)
  //       .then((response) {
  //     if (response.result == "true") {
  //       // var snackBar = SnackBar(content: Text(response.msg));
  //       // _key.currentState.showSnackBar(snackBar);
  //       Toast.show(response.msg, context);
  //       Navigator.pop(context);
  //     } else {
  //       var snackBar = SnackBar(content: Text(response.msg));
  //       _key.currentState.showSnackBar(snackBar);
  //     }
  //   });
  // }

  Widget myTextField(
      TextEditingController controller,
      FocusNode fromNode,
      TextInputType type,
      String hint,
      String label,
      Icon prefix,
      IconButton suffix,
      bool obs,
      FocusNode toNode) {
    return TextField(
        style: TextStyle(color: Colors.white),
        controller: controller,
        focusNode: fromNode,
        keyboardType: type,
        obscureText: obs,
        onSubmitted: (value) {
          FocusScope.of(context).requestFocus(toNode);
        },
        decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.blue)),
            prefixIcon: prefix,
            suffixIcon: suffix,
            hintText: hint,
            hintStyle: TextStyle(color: Colors.blue[400]),
            labelText: label,
            labelStyle: TextStyle(color: Colors.blue[400])));
  }
}
