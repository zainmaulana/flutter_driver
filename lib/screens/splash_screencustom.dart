import 'dart:async';
import 'package:vmsdriver/screens/login_screens.dart';
import 'package:vmsdriver/screens/utama_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreenCustom extends StatefulWidget {
  static String tag = "splash-screen-custom";
  @override
  _SplashScreenCustomState createState() => _SplashScreenCustomState();
}

class _SplashScreenCustomState extends State<SplashScreenCustom> {
  @override
  void initState() {
    // TODO: implement initState
    startSplashScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "gambar/vms-icon.png",
              width: 300,
              height: 300,
            ),
            Text(
              "Vehicle Management System",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "for Driver",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CircularProgressIndicator(
              backgroundColor: Colors.white,
            )
          ],
        ),
      ),
    );
  }

  startSplashScreen() {
    var duration = Duration(seconds: 3);
    return Timer(duration, () async {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      bool session = (preferences.getBool('session') ?? false);
      if (session) {
        Navigator.pushReplacementNamed(context, UtamaScreen.tag);
      } else {
        Navigator.pushReplacementNamed(context, LoginScreen.tag);
      }
    });
  }
}
