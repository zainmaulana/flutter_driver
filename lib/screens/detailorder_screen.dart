import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmsdriver/helpers/general_helper.dart';
import 'package:vmsdriver/models/model_driverchat.dart';
import 'package:vmsdriver/models/model_drivergetdetail.dart';
import 'package:vmsdriver/network/network_ojol.dart';
import 'package:vmsdriver/screens/chatroom_screen.dart';
import 'package:vmsdriver/screens/utama_screen.dart';

class DetailOrderScreen extends StatefulWidget {
  static String tag = 'detailorder-page';
  DataDetailOrder dataDetailOrder;
  DetailOrderScreen({Key key, this.dataDetailOrder}) : super(key: key);

  @override
  _DetailOrderScreenState createState() => _DetailOrderScreenState();
}

class _DetailOrderScreenState extends State<DetailOrderScreen> {
  int iduser;
  String token, device;
  NetworkOjol networkOjol = NetworkOjol();
  String _dayNow;
  DateTime _selectedDate;
  String _dayOrder;
  DateTime _tempOrderDate;
  int _selisihJam;
  String monthKurang10;
  String dayKurang10;
  String _tahunNow;
  Timer _timer;
  int _start = 10;

  int startDateH;
  int startDateM;
  TimeOfDay _selectedTime;
  int totalNow;
  int totalStartDate;
  int totalStartDateY;
  int totalStartDateM;
  int totalStartDateD;

  DataChatDriver dataChatDriver;

  List<int> rotatedBox = [1, 2, 3, 4];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("My Order"),
      ),
      body: Container(
        child: tampilanDetailOrder(),
      ),
    );
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getId();
    print("ID User : " + iduser.toString());
  }

  Future<void> setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setInt("idorder", widget.dataDetailOrder.idOrder);
    print("ID Order : " + widget.dataDetailOrder.idOrder.toString());
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 3);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start > 1) {
            //Setting Waktu
            _selectedDate = DateTime.now();
            _tahunNow = _selectedDate.toLocal().year.toString();

            if (_selectedDate.month < 10) {
              monthKurang10 = "0" + _selectedDate.month.toString();
              if (_selectedDate.day < 10) {
                dayKurang10 = "0" + _selectedDate.day.toString();
                _dayNow = _tahunNow + "-" + monthKurang10 + "-" + dayKurang10;
              } else {
                _dayNow = _tahunNow +
                    "-" +
                    monthKurang10 +
                    "-" +
                    _selectedDate.day.toString();
              }
            } else if (_selectedDate.day < 10) {
              dayKurang10 = "0" + _selectedDate.day.toString();
              _dayNow = _tahunNow +
                  "-" +
                  _selectedDate.month.toString() +
                  "-" +
                  dayKurang10;
            } else {
              _dayNow = _tahunNow +
                  "-" +
                  _selectedDate.month.toString() +
                  "-" +
                  _selectedDate.day.toString();
            }

            _dayOrder = widget.dataDetailOrder.startDate.substring(0, 10);

            _tempOrderDate = DateTime.parse(widget.dataDetailOrder.startDate);

            _selisihJam = _tempOrderDate.difference(_selectedDate).inHours;

            _selectedTime = TimeOfDay.now();

            getChats();
            displayOrder();
            setStateButton();

            print("Refresh State");
          }
        },
      ),
    );
  }

  void displayOrder() {
    startDateH = int.parse(widget.dataDetailOrder.startDate.substring(11, 13));
    startDateM = int.parse(widget.dataDetailOrder.startDate.substring(14, 16));
    startDateM = startDateM + 1; //Tambahan Waktu 15 Menit

    totalNow = _selectedDate.year + _selectedDate.month + _selectedDate.day;
    totalStartDateY =
        int.parse(widget.dataDetailOrder.startDate.substring(0, 4));
    totalStartDateM =
        int.parse(widget.dataDetailOrder.startDate.substring(5, 7));
    totalStartDateD =
        int.parse(widget.dataDetailOrder.startDate.substring(8, 10));
    totalStartDate = totalStartDateY + totalStartDateM + totalStartDateD;

    // print("ID Order : " + widget.dataDetailOrder.idOrder.toString());
    // print("Start Day : " + widget.dataDetailOrder.startDate.substring(0, 13));
    // print("Hari Sekarang : " + _selectedDate.toString().substring(0, 13));
    // print("Start Hour : " + startDateH.toString() + startDateM.toString());
    // print("Waktu Hour : " + _selectedTime.hour.toString());
    // print("Waktu Menit : " + _selectedTime.minute.toString());
    print("Total Now : " + totalNow.toString());
    print("Total StartDate : " + totalStartDate.toString());

    //Cek Batas Waktu
    if (totalStartDate == totalNow) {
      if (startDateH == _selectedTime.hour) {
        if (startDateM < _selectedTime.minute) {}
      }
    } else if (totalStartDate < totalNow) {
      // setState(() {
      //   Toast.show("Dibatalkan karena Tidak ada Konfirmasi/Terlambat", context);
      //   Navigator.of(context).pushNamedAndRemoveUntil(
      //       UtamaScreen.tag, (Route<dynamic> route) => false);
      //   _timer.cancel();
      // });
    }
  }

  void startOrder(String idOrder) {
    networkOjol
        .startOrder(
      idOrder,
    )
        .then((response) async {
      if (response.result == "true") {
        Toast.show(response.message, context);
        Navigator.of(context).pushReplacementNamed(
          DetailDriverScreen.tag,
          arguments: idOrder,
        );
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void getChats() {
    networkOjol
        .getChats(widget.dataDetailOrder.idOrder.toString())
        .then((response) {
      if (response.result == "true") {
        print("Nama Karyawan : " + response.data[0].namaKaryawan);
        setState(() {
          dataChatDriver = response.data[0];
        });
      } else {
        print("Tidak Ada Hasil");
        setState(() {
          dataChatDriver = null;
        });
      }
    });
  }

  Widget setStateButton() {
    print("Day Order : " + _dayOrder);
    print("Day Now : " + _dayNow);
    print("Selisih Jam : " + _selisihJam.toString());
    if (_dayOrder == _dayNow) {
      if (_selisihJam < 3) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 5),
          child: MaterialButton(
            minWidth: double.infinity,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
              side: BorderSide(color: Colors.blue[900]),
            ),
            color: Colors.blue[900],
            textColor: Colors.white,
            padding: EdgeInsets.all(10),
            onPressed: () {
              startOrder(
                widget.dataDetailOrder.idOrder.toString(),
              );
            },
            child: Text(
              "Start Order",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
      } else {
        return nullButton();
      }
    } else {
      return nullButton();
    }
  }

  Widget nullButton() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 10, 20, 5),
      child: MaterialButton(
        minWidth: double.infinity,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: BorderSide(color: Colors.black),
        ),
        padding: EdgeInsets.all(10),
        onPressed: null,
        child: Text(
          "Start Order",
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget myTextField(
    TextEditingController controller,
    TextInputType type,
    String hint,
    String label,
    Icon prefix,
    IconButton suffix,
    bool readOnly,
  ) {
    return TextField(
      style: TextStyle(
        color: Colors.black,
      ),
      controller: controller,
      keyboardType: type,
      readOnly: readOnly,
      decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.blue[900],
          )),
          prefixIcon: prefix,
          suffixIcon: suffix,
          hintText: hint,
          hintStyle: TextStyle(color: Colors.black),
          labelText: label,
          labelStyle: TextStyle(color: Colors.black)),
    );
  }

  Widget tampilanDetailOrder() {
    return Column(
      children: [
        Container(
          height: 120,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hi, This is your report order",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.blue[900],
                  ),
                ),
                Text(
                  "Make sure to contact user to confirm your order's origin and destination",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
                Text(
                  "Please read Important Notice for more information",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
          child: Flexible(
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Order No #" + widget.dataDetailOrder?.orderCode ?? "Kosong",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey[700],
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 400,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Icon(
                      Icons.trip_origin,
                      size: 30,
                      color: Colors.red[700],
                    ),
                  ),
                  for (var i in rotatedBox)
                    RotatedBox(
                      quarterTurns: 1,
                      child: Icon(
                        Icons.remove,
                        size: 30,
                        color: Colors.red[700],
                      ),
                    ),
                  Flexible(
                    child: Icon(
                      Icons.place,
                      size: 30,
                      color: Colors.green,
                    ),
                  ),
                ],
              )),
              Expanded(
                flex: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Flexible(
                        child: Text(
                          widget.dataDetailOrder?.pickupLocation ?? "Kosong",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.watch_later_outlined,
                          size: 20,
                          color: Colors.red[700],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: Text(
                            widget.dataDetailOrder?.startDate ?? "Kosong",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.group,
                          size: 20,
                          color: Colors.red[700],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: Text(
                            widget.dataDetailOrder?.jumlahPenumpang
                                    .toString() ??
                                "Kosong",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.notes,
                          size: 20,
                          color: Colors.red[700],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: Text(
                            widget.dataDetailOrder?.keperluan ?? "Kosong",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: Flexible(
                        child: Text(
                          widget.dataDetailOrder?.dropLocation ?? "Kosong",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Flexible(
                        child: Text(
                          "Catatan : " + widget.dataDetailOrder?.catatan ??
                              "Kosong",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 8),
          child: Flexible(
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "User Information",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey[700],
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        Flexible(
          child: Container(
            height: 100,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 30,
                        backgroundColor: Colors.grey[400],
                        backgroundImage: AssetImage('gambar/vms-icon2.png'),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.dataDetailOrder?.namaKaryawan ?? "Kosong",
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              widget.dataDetailOrder?.namaDivisi ?? "Kosong",
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              widget.dataDetailOrder?.nikKaryawan ?? "Kosong",
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: IconButton(
                          icon: Icon(
                            Icons.message,
                            color: Colors.blue[900],
                            size: 40,
                          ),
                          onPressed: () {
                            setPref();
                            Navigator.of(context).pushNamed(ChatRoom.tag,
                                arguments: dataChatDriver);
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        setStateButton()
      ],
    );
  }
}
