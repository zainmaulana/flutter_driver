import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmsdriver/helpers/general_helper.dart';
import 'package:vmsdriver/models/model_drivernotification.dart';
import 'package:vmsdriver/network/network_ojol.dart';
import 'package:vmsdriver/screens/utama_screen.dart';

class DetailNotif extends StatefulWidget {
  static String tag = "detailnotif";
  DataNotifDriver dataNotifDriver;

  DetailNotif({Key key, this.dataNotifDriver}) : super(key: key);

  @override
  _DetailNotifState createState() => _DetailNotifState();
}

class _DetailNotifState extends State<DetailNotif> {
  int nilai = 0;
  int iduser, iddriver;
  String token, device;
  NetworkOjol networkOjol = NetworkOjol();

  Timer _timer;
  int _start = 10;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
    startTimer();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    iddriver = pref.getInt("iddriver");
    device = await getId();
    print("ID User : " + iduser.toString());
    print("ID Notif : " + widget.dataNotifDriver.idNotif.toString());
    print("ID Order Type : " + widget.dataNotifDriver.idOrderType.toString());
  }

  void acceptOrder(String idOrder) {
    networkOjol
        .acceptOrder(
      idOrder,
      iduser.toString(),
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        if (widget.dataNotifDriver.idOrderType == 1) {
          Navigator.of(context).pushReplacementNamed(
            DetailDriverScreen.tag,
            arguments: idOrder,
          );
        } else if (widget.dataNotifDriver.idOrderType == 2) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              UtamaScreen.tag, (Route<dynamic> route) => false);
        }
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void deleteNotif(
    String idNotif,
  ) {
    networkOjol
        .deleteNotifUser(
      idNotif,
    )
        .then((response) async {
      if (response.result == "true") {
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        Toast.show(response.message, context);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail " + widget.dataNotifDriver.orderCode),
      ),
      body: tampilanDetailNotif(),
    );
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start > 1) {
            // timer.cancel();
            setStateButtonNotif();
            print("Refresh State");
          }
        },
      ),
    );
  }

  Widget setStateButtonNotif() {
    if (widget.dataNotifDriver.idDriverOrder == null &&
        widget.dataNotifDriver.idOrderStatus == 1) {
      return Container(
        height: 50,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                color: Colors.red[700],
                textColor: Colors.white,
                padding: EdgeInsets.all(5),
                onPressed: () {
                  if (widget.dataNotifDriver.idDriverOrder == null) {
                    print(widget.dataNotifDriver.idNotif.toString());
                    deleteNotif(
                      widget.dataNotifDriver.idNotif.toString(),
                    );
                  } else {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        UtamaScreen.tag, (Route<dynamic> route) => false);
                    Toast.show("Order Sudah Diambil", context);
                  }
                },
                child: Text(
                  "Tolak",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                width: 100,
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                color: Colors.green,
                textColor: Colors.white,
                padding: EdgeInsets.all(5),
                onPressed: () {
                  if (widget.dataNotifDriver.idDriverOrder == null) {
                    print(widget.dataNotifDriver.orderCode.toString());
                    acceptOrder(
                      widget.dataNotifDriver.idOrder.toString(),
                    );
                  } else {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        UtamaScreen.tag, (Route<dynamic> route) => false);
                    Toast.show("Order Sudah Diambil", context);
                  }
                },
                child: Text(
                  "Terima",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      if (iddriver == iduser) {
        return Container(
          height: 50,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Order Diambil Anda",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
              ],
            ),
          ),
        );
      } else {
        return Container(
          height: 50,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Order Sudah Diambil",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.red[700],
                  ),
                ),
                Container(
                  width: 100,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  color: Colors.red[700],
                  textColor: Colors.white,
                  padding: EdgeInsets.all(5),
                  onPressed: () {
                    print(widget.dataNotifDriver.idNotif);
                    deleteNotif(
                      widget.dataNotifDriver.idNotif.toString(),
                    );
                  },
                  child: Text(
                    "Hapus Notif",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
    }
  }

  Widget tampilanDetailNotif() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Icon(
                Icons.trip_origin,
                size: 30,
                color: Colors.red[700],
              ),
              Flexible(
                child: Text(
                  widget.dataNotifDriver.pickupLocation,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Icon(
                Icons.place,
                size: 30,
                color: Colors.green,
              ),
              Flexible(
                child: Text(
                  widget.dataNotifDriver.dropLocation,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Icon(
                Icons.person,
                size: 30,
                color: Colors.black,
              ),
              Flexible(
                child: Text(
                  widget.dataNotifDriver.namaKaryawan,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Icon(
                Icons.apartment,
                size: 30,
                color: Colors.black,
              ),
              Flexible(
                child: Text(
                  widget.dataNotifDriver.namaDivisi,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Flexible(
                child: Text(
                  "Tanggal Berangkat : " + widget.dataNotifDriver.startDate,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Text("Jarak"),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  " : " + widget.dataNotifDriver.realDistance + " km",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Text("Durasi"),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  " : " + widget.dataNotifDriver.realDuration + " Jam",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        setStateButtonNotif()
      ],
    );
  }
}
