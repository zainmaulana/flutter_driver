import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_6.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmsdriver/helpers/general_helper.dart';
import 'package:vmsdriver/models/model_driverchat.dart';
import 'package:vmsdriver/network/network_ojol.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

class ChatRoom extends StatefulWidget {
  static String tag = "chat-page";
  DataChatDriver dataChatDriver;
  ChatRoom({Key key, this.dataChatDriver}) : super(key: key);

  @override
  _ChatRoomState createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  Timer _timer;
  int _start = 10;
  final ScrollController listScrollController = new ScrollController();
  NetworkOjol networkOjol = NetworkOjol();
  List<DataChatDriver> dataChatDriver;
  int iduser, idorder;
  String token, device;
  final TextEditingController _chatText = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getPref();
    startTimer();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    idorder = pref.getInt("idorder");
    device = await getId();
    getChats();
  }

  getChats() {
    networkOjol.getChats(idorder.toString()).then((response) {
      if (response.result == "true") {
        print("ID Order : " + response.data[0].idOrder.toString());
        setState(() {
          dataChatDriver = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        setState(() {
          dataChatDriver = null;
        });
      }
    });
  }

  inputChat(
    String idOrder,
    String idUser,
    String chat,
  ) {
    networkOjol
        .inputChat(
      idOrder,
      idUser,
      chat,
    )
        .then((response) {
      if (response.result == "true") {
        Navigator.of(context).pushReplacementNamed(ChatRoom.tag,
            arguments: widget.dataChatDriver);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 3);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start > 1) {
            // timer.cancel();
            getChats();
            print("Refresh State");
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Chat ke " + widget.dataChatDriver.namaKaryawan,
        ),
      ),
      body: Stack(children: <Widget>[
        Column(
          children: [
            Flexible(
                child: ListView.builder(
              padding: EdgeInsets.all(10.0),
              itemCount: dataChatDriver?.length ?? 0,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    conversationWidget(index),
                  ],
                );
              },
              reverse: true,
              controller: listScrollController,
            )),
            inputWidget() // The input widget
          ],
        ),
      ]),
    );
  }

  Widget conversationWidget(int index) {
    if (dataChatDriver[index].idUser == iduser) {
      return Column(
        children: [
          ChatBubble(
            clipper: ChatBubbleClipper6(type: BubbleType.sendBubble),
            alignment: Alignment.topRight,
            margin: EdgeInsets.only(top: 20),
            backGroundColor: Colors.blue[900],
            child: Container(
              constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.7,
              ),
              child: Text(
                dataChatDriver[index].chat,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Text(
              dataChatDriver[index].tglChat,
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 12.0,
                  fontStyle: FontStyle.normal),
            ),
          ),
        ],
      );
    } else if (dataChatDriver[index].idUser != iduser) {
      return Column(
        children: [
          ChatBubble(
            clipper: ChatBubbleClipper6(type: BubbleType.receiverBubble),
            backGroundColor: Colors.red[700],
            margin: EdgeInsets.only(top: 20),
            child: Container(
              constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.7,
              ),
              child: Text(
                dataChatDriver[index].chat,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              dataChatDriver[index].tglChat,
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 12.0,
                  fontStyle: FontStyle.normal),
            ),
          ),
        ],
      );
    }
    return Container();
  }

  Widget inputWidget() {
    if (widget.dataChatDriver.idOrderStatus == 4 ||
        widget.dataChatDriver.idOrderStatus == 5 ||
        widget.dataChatDriver.idOrderStatus == 6) {
      return Container();
    } else {
      return Container(
        child: Row(
          children: <Widget>[
            Material(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: new IconButton(
                  icon: new Icon(Icons.phone),
                  onPressed: () => url_launcher
                      .launch('tel:' + widget.dataChatDriver.noTelpVs),
                  color: Colors.blue[900],
                ),
              ),
              color: Colors.white,
            ),
            Flexible(
              child: Container(
                child: TextField(
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15.0,
                  ),
                  controller: _chatText,
                  decoration: InputDecoration.collapsed(
                    hintText: 'Type a message',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ),
            Material(
              child: new Container(
                margin: new EdgeInsets.symmetric(horizontal: 10),
                child: new IconButton(
                  icon: new Icon(Icons.send),
                  onPressed: () {
                    print(widget.dataChatDriver.orderCode.toString());
                    inputChat(
                      idorder.toString(),
                      iduser.toString(),
                      _chatText.text,
                    );
                  },
                  color: Colors.blue[900],
                ),
              ),
              color: Colors.white,
            ),
          ],
        ),
        width: double.infinity,
        height: 50.0,
        decoration: new BoxDecoration(
            border: new Border(
                top: new BorderSide(
              color: Colors.blue[900],
              width: 0.5,
            )),
            color: Colors.white),
      );
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }
}
