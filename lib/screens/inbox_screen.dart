import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmsdriver/helpers/general_helper.dart';
import 'package:vmsdriver/models/model_driverchat.dart';
import 'package:vmsdriver/models/model_drivernotification.dart';
import 'package:vmsdriver/network/network_ojol.dart';
import 'package:vmsdriver/widgets/my_chat.dart';
import 'package:vmsdriver/widgets/my_notif.dart';

class InboxTabBar extends StatefulWidget {
  @override
  _InboxTabBarState createState() => _InboxTabBarState();
}

class _InboxTabBarState extends State<InboxTabBar>
    with TickerProviderStateMixin {
  NetworkOjol networkOjol = NetworkOjol();
  int iduser;
  String token, device;
  List<DataNotifDriver> dataNotifDriver;
  List<DataChatDriver> dataChatDriver;
  TabController controller;

  // Timer _timer;
  // int _start = 10;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: 2, vsync: this);
    controller.addListener(() {
      setState(() {
        switch (controller.index) {
          case 0:
            return getNotifications();
            break;
          case 1:
            return getChats();
            break;
        }
      });
    });
    getPref();
    // startTimer();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getId();
    print("ID User : " + iduser.toString());
    getNotifications();
    getChats();
  }

  // void startTimer() {
  //   const oneSec = const Duration(seconds: 3);
  //   _timer = new Timer.periodic(
  //     oneSec,
  //     (Timer timer) => setState(
  //       () {
  //         if (_start > 1) {
  //           getNotifications();
  //           getChats();

  //           print("Refresh State");
  //         }
  //       },
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(48),
        child: Material(
          color: Colors.white,
          child: TabBar(
            controller: controller,
            tabs: <Widget>[
              Tab(
                text: "NOTIFICATIONS",
              ),
              Tab(
                text: "MESSAGES",
              ),
            ],
            indicatorColor: Colors.blue[900],
            labelColor: Colors.blue[900],
          ),
        ),
      ),
      body: TabBarView(controller: controller, children: <Widget>[
        RefreshIndicator(
            child: Container(
              child: NotifDriver(
                dataNotifDriver: dataNotifDriver,
              ),
            ),
            onRefresh: () => getNotifications()),
        RefreshIndicator(
            child: Container(
              child: ChatDriver(
                dataChatDriver: dataChatDriver,
              ),
            ),
            onRefresh: () => getChats()),
      ]),
    );
  }

  getNotifications() {
    networkOjol.getNotifUser(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("ID User : " + response.data[0].idUser.toString());
        // Toast.show(response.message, context);
        setState(() {
          dataNotifDriver = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        // Toast.show(response.message, context);
        setState(() {
          dataNotifDriver = null;
        });
      }
    });
  }

  getChats() {
    networkOjol.getEachChat(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("ID User : " + response.data[0].idUser.toString());
        // Toast.show(response.message, context);
        setState(() {
          dataChatDriver = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        // Toast.show(response.message, context);
        setState(() {
          dataChatDriver = null;
        });
      }
    });
  }
}
