import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:vmsdriver/models/model_driverhistory.dart';
import 'package:flutter/material.dart';

class DetailHistory extends StatefulWidget {
  static String tag = "detailhistory";
  DataHistoryDriver dataHistoryDriver;

  DetailHistory({Key key, this.dataHistoryDriver}) : super(key: key);

  @override
  _DetailHistoryState createState() => _DetailHistoryState();
}

class _DetailHistoryState extends State<DetailHistory> {
  int nilai = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail " + widget.dataHistoryDriver?.orderCode ?? ""),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.trip_origin,
                  size: 30,
                  color: Colors.red[700],
                ),
                Flexible(
                  child: Text(
                    widget.dataHistoryDriver?.pickupLocation ?? "",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.place,
                  size: 30,
                  color: Colors.green,
                ),
                Flexible(
                  child: Text(
                    widget.dataHistoryDriver?.dropLocation ?? "",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Text("Jarak"),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Text(
                    " : " + widget.dataHistoryDriver?.realDistance ??
                        "" + " km",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Text("Durasi"),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Text(
                    " : " + widget.dataHistoryDriver?.realDuration ??
                        "" + " Jam",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Container(
            width: double.infinity,
            child: Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: tampilRating(),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget tampilRating() {
    if (widget.dataHistoryDriver.idRating == null &&
        widget.dataHistoryDriver.idOrderStatus == 4) {
      return Container();
    } else if (widget.dataHistoryDriver.idRating != null &&
        widget.dataHistoryDriver.idOrderStatus == 4) {
      return Column(
        children: [
          RatingBar(
            initialRating: widget.dataHistoryDriver.idRating.toDouble(),
            minRating: widget.dataHistoryDriver.idRating.toDouble(),
            direction: Axis.horizontal,
            itemCount: widget.dataHistoryDriver.idRating,
            ignoreGestures: true,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onRatingUpdate: (rating) {
              print(rating);
              setState(() {
                nilai = rating.toInt();
              });
            },
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              widget.dataHistoryDriver?.commentRating ?? "",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ],
      );
    } else if (widget.dataHistoryDriver.idRating == null &&
        widget.dataHistoryDriver.idOrderStatus == 5) {
      return Row(
        children: [
          Text("Komentar Batal"),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Text(
              " : " + widget.dataHistoryDriver?.commentCancel ?? "",
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ],
      );
    }
    return null;
  }
}
