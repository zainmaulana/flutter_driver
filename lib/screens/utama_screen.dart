import 'dart:async';
import 'dart:convert';
import 'package:vmsdriver/helpers/general_helper.dart';
import 'package:vmsdriver/models/model_drivergetdetail.dart';
import 'package:vmsdriver/models/model_driverhistory.dart';
import 'package:vmsdriver/models/model_getprofile.dart';
import 'package:vmsdriver/models/pin_pill_info.dart';
import 'package:vmsdriver/network/network_ojol.dart';
import 'package:vmsdriver/screens/inbox_screen.dart';
import 'package:vmsdriver/screens/login_screens.dart';
import 'package:vmsdriver/widgets/map_pin_pill.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmsdriver/widgets/my_account.dart';
import 'package:vmsdriver/widgets/my_order.dart';

final Map<String, Item> _items = <String, Item>{};
Item _itemForMessage(Map<String, dynamic> message) {
  final dynamic data = message['data'] ?? message;
  var datax = data['datax'];
  //  var click_action = data['click_action'];
  var dataxItem = jsonDecode(datax)['datax']['data'];
  DataHistoryDriver dataOrder = DataHistoryDriver.fromJson(dataxItem);
  String idOrder = dataOrder.idOrder.toString();
  print("ID Order : " + idOrder);
  // final String itemId = data['id'];
  final Item item = _items.putIfAbsent(
      idOrder,
      () => Item(
            itemId: idOrder,
            orderCode: dataOrder.orderCode,
            idOrderStatus: dataOrder.idOrderStatus.toString(),
            idOrderType: dataOrder.idOrderType.toString(),
            origin: dataOrder.pickupLocation,
            destination: dataOrder.dropLocation,
            distance: dataOrder.realDistance,
            duration: dataOrder.realDuration,
            latCustomerOri: dataOrder.pickupLatitude,
            lngCustomerOri: dataOrder.pickupLongitude,
            locCustomerOri: dataOrder.pickupLocation,
            latCustomerDest: dataOrder.dropLatitude,
            lngCustomerDest: dataOrder.dropLongitude,
            locCustomerDest: dataOrder.dropLocation,
          ))
    ..status = jsonDecode(datax)['datax']['result'];
  return item;
}

class Item {
  Item({
    this.itemId,
    this.orderCode,
    this.idOrderStatus,
    this.idOrderType,
    this.origin,
    this.destination,
    this.distance,
    this.duration,
    this.latCustomerOri,
    this.lngCustomerOri,
    this.locCustomerOri,
    this.latCustomerDest,
    this.lngCustomerDest,
    this.locCustomerDest,
  });
  final String itemId;

  StreamController<Item> _controller = StreamController<Item>.broadcast();
  Stream<Item> get onChanged => _controller.stream;
  String orderCode,
      idOrderStatus,
      idOrderType,
      origin,
      destination,
      distance,
      duration,
      latCustomerOri,
      lngCustomerOri,
      locCustomerOri,
      latCustomerDest,
      lngCustomerDest,
      locCustomerDest;
  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
  Route<void> get route {
    final String routeName = "detaildriverscreen";
    return routes.putIfAbsent(
      routeName,
      () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => DetailDriverScreen(
          itemId: itemId,
        ),
      ),
    );
  }
}

class UtamaScreen extends StatefulWidget {
  static String tag = "utama-page";
  @override
  _UtamaScreenState createState() => _UtamaScreenState();
}

class _UtamaScreenState extends State<UtamaScreen>
    with SingleTickerProviderStateMixin {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _homeScreenText = "Waiting for token...";
  String fcm;
  String tokenUpdate;
  int iduser;
  String token, device;
  NetworkOjol networkOjol = NetworkOjol();
  DataProfile dataProfile;
  DataDetailOrder dataDetailOrder;
  String idOrder;
  TabController controller;
  bool click = false;

  // Timer _timer;
  // int _start = 10;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = TabController(length: 3, vsync: this);
    controller.addListener(() {
      setState(() {
        switch (controller.index) {
          case 0:
            return getDetailOrder();
            break;
          case 1:
            return;
            break;
          case 2:
            return getProfile();
            break;
        }
      });
    });
    getPref();
    // startTimer();

    //Firebase Configuration
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showItemDialog(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // _navigateToItemDetail(message);
        _showItemDialog(message);

        // if (click == true) {
        //   _showItemDialog(message);
        // }
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // _navigateToItemDetail(message);
        // _showItemDialog(message);
        _showItemDialog(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: true),
    );
    _firebaseMessaging.onIosSettingsRegistered.listen(
      (IosNotificationSettings settings) {
        print("Settings registered: $settings");
      },
    );
    //Function untuk Get Token FCM dari Firebase
    _firebaseMessaging.getToken().then(
      (String token) {
        assert(token != null);
        setState(() {
          tokenUpdate = token;
          _homeScreenText = "Push Messaging Token : $token";
        });
        print(_homeScreenText);
        setTokentoPref(tokenUpdate);
        insertTokentoDb();
      },
    );
  }

  //Get Pref untuk Cek FCM
  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    fcm = pref.getString("fcm") ?? '';
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getId();
    print("ID User : " + iduser.toString());
    getProfile();
    getDetailOrder();
  }

  // void startTimer() {
  //   const oneSec = const Duration(seconds: 1);
  //   _timer = new Timer.periodic(
  //     oneSec,
  //     (Timer timer) => setState(
  //       () {
  //         if (_start > 1) {
  //           getProfile();
  //           getDetailOrder();

  //           print("Refresh State");
  //         }
  //       },
  //     ),
  //   );
  // }

  //boolean Show Alert Dialog jika ada Order Masuk
  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (_) => _buildDialog(context, _itemForMessage(message)),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  //Show Alert Dialog jika ada Order Masuk
  Widget _buildDialog(BuildContext context, Item item) {
    return AlertDialog(
      content: Text(
        "Order : ${item.orderCode} Datang !",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Icon(Icons.person_pin_circle),
                    Flexible(
                        child: Text(
                      "Origin : " + item.origin,
                      overflow: TextOverflow.ellipsis,
                    ))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Icon(Icons.person_pin_circle),
                    Flexible(
                        child: Text(
                      "Destination : " + item.destination,
                      overflow: TextOverflow.ellipsis,
                    ))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Jarak : " + item.distance),
                    SizedBox(
                      width: 10,
                    ),
                    Text("Durasi : " + item.duration)
                  ],
                ),
              ),
              Row(
                children: [
                  FlatButton(
                    child: const Text('Tolak'),
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  FlatButton(
                    child: const Text('Terima'),
                    onPressed: () {
                      if (item.idOrderType == "1") {
                        insertLokasi(item.itemId);
                        acceptOrder(item.itemId);
                        Navigator.of(context).pushReplacementNamed(
                          DetailDriverScreen.tag,
                          arguments: item.itemId,
                        );
                      } else if (item.idOrderType == "2") {
                        acceptOrder(item.itemId);
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            UtamaScreen.tag, (Route<dynamic> route) => false);
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  //Navigasi ke halaman detail ketika tombol (ex:show) ditekan
  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("My Home"),
        actions: [
          IconButton(
              icon: Icon(Icons.logout),
              onPressed: () async {
                SharedPreferences pref = await SharedPreferences.getInstance();
                pref.clear();
                Navigator.popAndPushNamed(context, LoginScreen.tag);
              })
        ],
      ),
      body: Center(
        child: TabBarView(controller: controller, children: <Widget>[
          RefreshIndicator(
              child: Container(
                child: MyOrder(
                  dataDetailOrder: dataDetailOrder,
                  idOrder: idOrder,
                ),
              ),
              onRefresh: () => getDetailOrder()),
          InboxTabBar(),
          RefreshIndicator(
              child: Container(
                child: MyAccount(
                  dataProfile: dataProfile,
                ),
              ),
              onRefresh: () => getProfile()),
        ]),
      ),
      bottomNavigationBar: Material(
        color: Colors.white,
        child: TabBar(
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.post_add),
              text: "My Order",
            ),
            Tab(
              icon: Icon(Icons.message),
              text: "My Inbox",
            ),
            Tab(
              icon: Icon(Icons.account_box),
              text: "My Account",
            ),
          ],
          controller: controller,
          indicatorColor: Colors.blue[900],
          labelColor: Colors.blue[900],
        ),
      ),
    );
  }

  getProfile() {
    networkOjol.getProfileUser(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("Email User : " + response.data.email);
        // Toast.show(response.message, context);
        setState(() {
          dataProfile = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        // Toast.show(response.message, context);
        setState(() {
          dataProfile = null;
        });
      }
    });
  }

  getDetailOrder() {
    networkOjol.getDetailOrderDriver(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("Order : " + response.data.orderCode);
        // Toast.show(response.message, context);
        setState(() {
          dataDetailOrder = response.data;
          idOrder = "true";
        });
      } else {
        print("Tidak Ada Hasil");
        // Toast.show(response.message, context);
        setState(() {
          dataDetailOrder = null;
          idOrder = "false";
        });
      }
    });
  }

  //Fungsi untuk set Token FCM jika sebelumnya kosong
  setTokentoPref(String tokenUpdate) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("FCM : ", tokenUpdate);
    print("Token Anda :" + tokenUpdate);
  }

  //Fungsi untuk Insert Token FCM ke DB
  void insertTokentoDb() {
    networkOjol
        .registerFcm(
      iduser.toString(),
      tokenUpdate,
    )
        .then((response) {
      if (response.result == "true") {
        print("Berhasil Insert FCM : " + tokenUpdate);
        //Toast.show(response.message, context);
      } else {
        print("Gagal Insert FCM");
        Toast.show(response.message, context);
      }
    });
  }

  //Fungsi untuk Ambil Order
  acceptOrder(String idOrder1) {
    networkOjol
        .acceptOrder(
      idOrder1,
      iduser.toString(),
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  //Insert Pertama dari Driver
  insertLokasi(String idOrder2) {
    networkOjol
        .insertLokasi(
      idOrder2,
      iduser.toString(),
      "-6.2454252",
      "106.8709812",
      "Kantor Pusat PT. Hutama Karya",
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        print("Berhasil Update Lokasi");
      } else {
        Toast.show(response.message, context);
        print("Gagal Update Lokasi");
      }
    });
  }
}

//Halaman Detail Driver setelah Klik Terima Order
const double CAMERA_ZOOM = 16;
const double CAMERA_TILT = 80;
const double CAMERA_BEARING = 30;
const LatLng SOURCE_LOCATION = LatLng(-6.2969516, 106.6962871);

class DetailDriverScreen extends StatefulWidget {
  static String tag = "detaildriverscreen";
  String itemId;
  DetailDriverScreen({Key key, this.itemId}) : super(key: key);

  @override
  _DetailDriverScreenState createState() => _DetailDriverScreenState();
}

class _DetailDriverScreenState extends State<DetailDriverScreen>
    with AutomaticKeepAliveClientMixin {
  // Item _item;
  StreamSubscription<Item> _subscription;
  Map<String, Item> itemsbaru = <String, Item>{};
  Timer _timer;
  int _start = 10;
  NetworkOjol networkOjol = NetworkOjol();
  DataDetailOrder dataDetailOrder;

  String latCustomer;
  String lngCustomer;
  String imageDriver;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = Set<Marker>();
// for my drawn routes on the map
  Set<Polyline> _polylines = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints;
  String googleAPIKey =
      'AIzaSyB87Z7EaGa873QQ6RrF5QaAA5E42pxu-jI'; //AIzaSyA961qUUYeU2tnBuk4gS1fpiXVjnCFnbcQ
// for my custom marker pins
  BitmapDescriptor sourceIcon;
  BitmapDescriptor destinationIcon;
// the user's initial location and current location
// as it moves
  LocationData currentLocation;
// a reference to the destination location
  LocationData destinationLocation;
// wrapper around the location API
  Location location;
  double pinPillPosition = -150;
  PinInformation currentlySelectedPin = PinInformation(
    pinPath: '',
    avatarPath: '',
    location: LatLng(0, 0),
    locationName: '',
    labelColor: Colors.grey,
  );
  PinInformation sourcePinInfo;
  PinInformation destinationPinInfo;

  String oriAddress;
  String destAddress;
  int iduser;
  String token, device;
  String idOrder, latcos, lngcos, idOrderStatus, orderCode;

  String namaDriver;
  String namaKaryawan;
  String unitKendaraan;
  String nopolUnit;

  @override
  void initState() {
    super.initState();
    getDataPref();
    // create an instance of Location
    location = new Location();
    polylinePoints = PolylinePoints();

    // if (_items[widget.itemId] != null) {
    //   _item = _items[widget.itemId];
    //   _subscription = _item.onChanged.listen((Item item) {
    //     if (!mounted) {
    //       _subscription.cancel();
    //     } else {
    //       setState(() {
    //         _item = item;
    //       });
    //     }
    //   });
    // }

    // Toast.show(_item.itemId, context);
    // subscribe to changes in the user's location
    // by "listening" to the location's onLocationChanged event
    location.onLocationChanged.listen((LocationData cLoc) {
      // cLoc contains the lat and long of the
      // current user's position in real time,
      // so we're holding on to it
      currentLocation = cLoc;
      updatePinOnMap();
    });
    // set custom marker pins
    setSourceAndDestinationIcons();
    // set the initial location
    startTimer();
    getPref();
  }

  getDetailOrder() {
    networkOjol.getDetailOrderDriver(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("Order : " + response.data.orderCode);
        Toast.show(response.message, context);
        setState(() {
          dataDetailOrder = response.data;
          imageDriver = "https://vms.hutamakarya.com/image_driver/" +
              response.data.imageDriver;
          namaDriver = response.data.namaDriver;
          namaKaryawan = response.data.namaKaryawan;
          unitKendaraan = response.data.namaUnit;
          nopolUnit = response.data.nopolUnit;
          orderCode = response.data.orderCode;
        });
      } else {
        print("Tidak Ada Hasil");
        Toast.show(response.message, context);
        setState(() {
          dataDetailOrder = null;
        });
      }
    });
  }

  Future<void> getDataPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    // var dataitems = pref.getString("items");
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getId();
    idOrder = pref.getString("idOrder");
    latcos = pref.getString("lat");
    lngcos = pref.getString("lng");
    getDetailOrder();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    idOrder = pref.getString("idOrder") ?? '';

    if (idOrder == null) {
      pref.setString("idOrder", dataDetailOrder.idOrder.toString());
      pref.setString("lat", latCustomer);
      pref.setString("lng", lngCustomer);
      print("berhasil:simpan");
    } else {
      print("berhasil:gagal");
    }
  }

  void updatePinOnMap() async {
    // create a new CameraPosition instance
    // every time the location changes, so the camera
    // follows the pin as it moves with an animation
    CameraPosition cPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
    );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
    // do this inside the setState() so Flutter gets notified
    // that a widget update is due
    setState(() {
      // updated position
      var pinPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);

      sourcePinInfo.location = pinPosition;

      // the trick is to remove the marker (by id)
      // and add it again at the updated location
      _markers.removeWhere((m) => m.markerId.value == 'sourcePin');
      _markers.add(Marker(
          markerId: MarkerId('sourcePin'),
          onTap: () {
            setState(() {
              currentlySelectedPin = sourcePinInfo;
              pinPillPosition = 0;
            });
          },
          position: pinPosition, // updated position
          icon: sourceIcon));
    });
  }

  void setSourceAndDestinationIcons() async {
    BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
            'gambar/marker_start.png')
        .then((onValue) {
      sourceIcon = onValue;
    });

    BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
            'gambar/marker_destination.png')
        .then((onValue) {
      destinationIcon = onValue;
    });
  }

  checkLokasiUser() {
    setState(() {
      idOrderStatus = dataDetailOrder.idOrderStatus.toString();
    });

    if (idOrderStatus == "2") {
      setState(() {
        latCustomer = dataDetailOrder?.pickupLatitude ?? latcos;
        lngCustomer = dataDetailOrder?.pickupLongitude ?? lngcos;
      });
    } else if (idOrderStatus == "3") {
      setState(() {
        latCustomer = dataDetailOrder?.dropLatitude ?? latcos;
        lngCustomer = dataDetailOrder?.dropLongitude ?? lngcos;
      });
    }

    print("lat: $latCustomer \n lng: $lngCustomer");
    showPinsOnMap();
    print("Cek Lokasi User");
  }

  void setInitialLocation() async {
    // set the initial location by pulling the user's
    // current location from the location's getLocation()
    currentLocation = await location.getLocation();

    // hard-coded destination for this example
    destinationLocation = LocationData.fromMap({
      "latitude": double.parse(latCustomer),
      "longitude": double.parse(lngCustomer),
    });
    print("Lat Lng Masuk : " +
        LatLng(
          double.parse(latCustomer),
          double.parse(lngCustomer),
        ).toString());
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 10);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start > 1) {
            checkLokasiUser();
            print("Refresh Lokasi");
            setInitialLocation();
            insertLokasi();
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    CameraPosition initialCameraPosition = CameraPosition(
        zoom: CAMERA_ZOOM,
        tilt: CAMERA_TILT,
        bearing: CAMERA_BEARING,
        target: SOURCE_LOCATION);
    if (currentLocation != null) {
      initialCameraPosition = CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: CAMERA_ZOOM,
          tilt: CAMERA_TILT,
          bearing: CAMERA_BEARING);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(orderCode ?? ""),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
              myLocationEnabled: true,
              compassEnabled: true,
              tiltGesturesEnabled: false,
              markers: _markers,
              polylines: _polylines,
              mapType: MapType.normal,
              initialCameraPosition: initialCameraPosition,
              onTap: (LatLng loc) {
                pinPillPosition = -150;
              },
              onMapCreated: (GoogleMapController controller) {
                // controller.setMapStyle(Utils.mapStyles);
                _controller.complete(controller);
                // my map has completed being created;
                // i'm ready to show the pins on the map
                showPinsOnMap();
              }),
          //Dikopikan ke Project User
          Positioned(
            top: 8,
            left: 60,
            right: 60,
            child: setStateDriver(),
          ),
          sourcePinInfo == null
              ? Center(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    Text("Memuat Lokasi Anda dan Customer"),
                  ],
                ))
              : Container(),
          MapPinPillComponent(
              pinPillPosition: pinPillPosition,
              currentlySelectedPin: currentlySelectedPin)
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget setStateDriver() {
    if (idOrderStatus == "2") {
      return RaisedButton(
        onPressed: () {
          startOrder(dataDetailOrder.idOrder.toString());
        },
        color: Colors.blue[900],
        child: Text(
          "Start Order",
          style: TextStyle(color: Colors.white),
        ),
      );
    } else if (idOrderStatus == "3") {
      return RaisedButton(
        onPressed: () {
          completeOrderNew(dataDetailOrder.idOrder.toString());
        },
        color: Colors.blue[900],
        child: Text(
          "Complete Order",
          style: TextStyle(color: Colors.white),
        ),
      );
    }
    return Container();
  }

  Future<void> showPinsOnMap() async {
    // get a LatLng for the source location
    // from the LocationData currentLocation object
    var pinPosition =
        LatLng(currentLocation.latitude, currentLocation.longitude);
    // get a LatLng out of the LocationData object
    var destPosition =
        LatLng(destinationLocation.latitude, destinationLocation.longitude);
    print("masuk");
    oriAddress = await getCurrentAddress();
    destAddress = await getDestAddress();
    sourcePinInfo = PinInformation(
      locationName: oriAddress,
      location: SOURCE_LOCATION,
      pinPath: "https://vms.hutamakarya.com/image_asset/marker_start.png",
      avatarPath: imageDriver,
      labelColor: Colors.blue[900],
      name: namaDriver,
      unitKendaraan: unitKendaraan,
      nopolUnit: nopolUnit,
    );

    destinationPinInfo = PinInformation(
      locationName: destAddress,
      location: LatLng(
        double.parse(latCustomer),
        double.parse(lngCustomer),
      ),
      pinPath: "https://vms.hutamakarya.com/image_asset/marker_destination.png",
      avatarPath: "https://vms.hutamakarya.com/image_asset/vms-icon2.png",
      labelColor: Colors.red[700],
      name: namaKaryawan,
    );

    print("lat:" + latCustomer + "\nlong:" + lngCustomer);
    print(LatLng(
      double.parse(latCustomer),
      double.parse(lngCustomer),
    ).toString());

    // add the initial source location pin
    _markers.add(Marker(
        markerId: MarkerId('sourcePin'),
        position: pinPosition,
        onTap: () {
          setState(() {
            currentlySelectedPin = sourcePinInfo;
            pinPillPosition = 0;
          });
        },
        icon: sourceIcon));
    // destination pin
    _markers.add(Marker(
        markerId: MarkerId('destPin'),
        position: destPosition,
        onTap: () {
          setState(() {
            currentlySelectedPin = destinationPinInfo;
            pinPillPosition = 80;
          });
        },
        icon: destinationIcon));
    // set the route lines on the map from source to destination
    // for more info follow this tutorial

    setPolylines();
  }

  void setPolylines() async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      googleAPIKey,
      PointLatLng(currentLocation.latitude, currentLocation.longitude),
      PointLatLng(destinationLocation.latitude, destinationLocation.longitude),
    );
    polylineCoordinates.clear();
    if (result != null) {
      print("Results not null");
      if (result.points.isNotEmpty) {
        result.points.forEach((PointLatLng point) {
          polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });

        setState(() {
          if (result != null) {
            print("Results not null");
            if (result.points.isNotEmpty) {
              _polylines.add(Polyline(
                  width: 2, // set the width of the polylines
                  polylineId: PolylineId("poly"),
                  color: Color.fromARGB(255, 40, 122, 198),
                  points: polylineCoordinates));
            }
          }
        });
      }
    }
  }

  Future<String> getCurrentAddress() async {
    final coordinates = Coordinates(
      currentLocation.latitude,
      currentLocation.longitude,
    );
    var address =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstLocation = address.first;
    return firstLocation.addressLine;
  }

  Future<String> getDestAddress() async {
    final coordinates = Coordinates(
      destinationLocation.latitude,
      destinationLocation.longitude,
    );
    var address =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstLocation = address.first;
    return firstLocation.addressLine;
  }

  void startOrder(String idOrder3) {
    networkOjol
        .startOrder(
      idOrder3,
    )
        .then((response) async {
      if (response.result == "true") {
        Toast.show(response.message, context);
        Navigator.of(context).pushReplacementNamed(
          DetailDriverScreen.tag,
          arguments: idOrder3,
        );
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  //Complete Order New dari Driver
  void completeOrderNew(String idOrder4) {
    networkOjol
        .completeOrderNew(
      idOrder4,
      iduser.toString(),
    )
        .then((response) async {
      if (response.result == "true") {
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.remove("idOrder");
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  //Complete Order dari Driver
  void completeOrder(String idOrder5) {
    networkOjol
        .completeOrder(
      idOrder5,
      iduser.toString(),
    )
        .then((response) async {
      if (response.result == "true") {
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.remove("idOrder");
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  //Insert Titik Terbaru dari Driver saat Perjalanan
  insertLokasi() {
    networkOjol
        .insertLokasi(
      dataDetailOrder?.idOrder.toString() ?? idOrder,
      iduser.toString(),
      currentLocation.latitude.toString(),
      currentLocation.longitude.toString(),
      oriAddress,
    )
        .then((response) {
      if (response.result == "true") {
        print("Berhasil Update Lokasi");
      } else {
        print("Gagal Update Lokasi");
      }
    });
  }
}
