import 'dart:async';

import 'package:vmsdriver/helpers/general_helper.dart';
import 'package:vmsdriver/models/model_driverhistory.dart';
import 'package:vmsdriver/network/network_ojol.dart';
import 'package:vmsdriver/widgets/my_history.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryScreen extends StatefulWidget {
  static String tag = "history-page";

  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen>
    with SingleTickerProviderStateMixin {
  NetworkOjol networkOjol = NetworkOjol();
  List<DataHistoryDriver> dataHistoryDriver;
  int iduser;
  String token, device;
  TabController controller;

  // Timer _timer;
  // int _start = 10;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = TabController(length: 2, vsync: this);
    controller.addListener(() {
      setState(() {
        switch (controller.index) {
          case 0:
            return getHistory("4");
            break;
          case 1:
            return getHistory("5");
            break;
        }
      });
    });
    getPref();
    // startTimer();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getId();
    print("Token : " + token + "\n Device : " + device);
    getHistory("4");
  }

  // void startTimer() {
  //   const oneSec = const Duration(seconds: 1);
  //   _timer = new Timer.periodic(
  //     oneSec,
  //     (Timer timer) => setState(
  //       () {
  //         if (_start > 1) {
  //           getHistory("4");
  //           getHistory("5");

  //           print("Refresh State");
  //         }
  //       },
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Order History"),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(48),
          child: Material(
            color: Colors.white,
            child: TabBar(
              controller: controller,
              tabs: <Widget>[
                Tab(
                  text: "COMPLETE",
                ),
                Tab(
                  text: "CANCEL",
                ),
              ],
              indicatorColor: Colors.blue[900],
              labelColor: Colors.blue[900],
            ),
          ),
        ),
      ),
      body: TabBarView(controller: controller, children: <Widget>[
        RefreshIndicator(
            child: Container(
              margin: EdgeInsets.all(10),
              child: History(
                dataHistoryDriver: dataHistoryDriver,
              ),
            ),
            onRefresh: () => getHistory("4")),
        RefreshIndicator(
            child: Container(
              margin: EdgeInsets.all(10),
              child: History(
                dataHistoryDriver: dataHistoryDriver,
              ),
            ),
            onRefresh: () => getHistory("5")),
      ]),
    );
  }

  getHistory(String status) async {
    networkOjol.getHistoryOrder(iduser.toString(), status).then((response) {
      if (response.result == "true") {
        print("hasil :" + response.data[0].idOrderStatus.toString());
        setState(() {
          dataHistoryDriver = response.data;
        });
      } else {
        print("hasil : tidak ada");
        setState(() {
          dataHistoryDriver = null;
        });
      }
    });
  }
}
