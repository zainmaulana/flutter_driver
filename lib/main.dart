import 'package:vmsdriver/screens/chatroom_screen.dart';
import 'package:vmsdriver/screens/detailnotif.dart';
import 'package:vmsdriver/screens/detailorder_screen.dart';
import 'package:vmsdriver/screens/splash_screencustom.dart';
import 'package:vmsdriver/screens/detailhistory.dart';
import 'package:vmsdriver/screens/history_screen.dart';
import 'package:vmsdriver/screens/login_screens.dart';
import 'package:vmsdriver/screens/register_screens.dart';
import 'package:vmsdriver/screens/utama_screen.dart';
import 'package:flutter/material.dart';
import 'screens/splash_screencustom.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    SplashScreenCustom.tag: (context) => SplashScreenCustom(),
    LoginScreen.tag: (context) => LoginScreen(),
    UtamaScreen.tag: (context) => UtamaScreen(),
    RegisterScreen.tag: (context) => RegisterScreen(),
    //WaitingDriverScreen.tag: (context) => WaitingDriverScreen()
    //DetailDriverScreen.tag: (context) => DetailDriverScreen()
    HistoryScreen.tag: (context) => HistoryScreen(),
    // DetailHistory.tag: (context) => DetailHistory()
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.blue[900],
        scaffoldBackgroundColor: Colors.grey[200],
        appBarTheme: AppBarTheme(
          color: Colors.blue[900],
        ),
      ),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        if (settings.name == DetailDriverScreen.tag) {
          return MaterialPageRoute(builder: (context) {
            return DetailDriverScreen(
              itemId: settings.arguments,
            );
          });
        } else if (settings.name == DetailHistory.tag) {
          return MaterialPageRoute(builder: (context) {
            return DetailHistory(
              dataHistoryDriver: settings.arguments,
            );
          });
        } else if (settings.name == DetailNotif.tag) {
          return MaterialPageRoute(builder: (context) {
            return DetailNotif(
              dataNotifDriver: settings.arguments,
            );
          });
        } else if (settings.name == DetailOrderScreen.tag) {
          return MaterialPageRoute(builder: (context) {
            return DetailOrderScreen(
              dataDetailOrder: settings.arguments,
            );
          });
        } else if (settings.name == ChatRoom.tag) {
          return MaterialPageRoute(builder: (context) {
            return ChatRoom(
              dataChatDriver: settings.arguments,
            );
          });
        }
      },
      home: SplashScreenCustom(),
      routes: routes,
    );
  }
}
