class ModelAcceptOrder {
  String result;
  String message;

  ModelAcceptOrder({this.result, this.message});

  ModelAcceptOrder.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    return data;
  }
}
