class ModelNotificationDriver {
  String result;
  String message;
  List<DataNotifDriver> data;

  ModelNotificationDriver({this.result, this.message, this.data});

  ModelNotificationDriver.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataNotifDriver>();
      json['data'].forEach((v) {
        data.add(new DataNotifDriver.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataNotifDriver {
  int idNotif;
  int idUser;
  String notif;
  String createdAt;
  int idOrder;
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String password;
  String rememberToken;
  String updatedAt;
  int idRole;
  int idKaryawan;
  int idDriver;
  String fcmToken;
  String device;
  bool bookingStatus;
  int idOrderStatus;
  int idOrderType;
  int idDriverOrder;
  String orderCode;
  String orderDate;
  String lastUpdatedOn;
  int jumlahPenumpang;
  String keperluan;
  String pickupDate;
  String pickupLatitude;
  String pickupLongitude;
  String pickupLocation;
  String dropDate;
  String dropLatitude;
  String dropLongitude;
  String dropLocation;
  String realDistance;
  String realDuration;
  bool isGanjilGenap;
  int idRating;
  String commentRating;
  String cancelDate;
  String commentCancel;
  String startDate;
  String catatan;
  int idDivisi;
  String namaKaryawan;
  String nikKaryawan;
  bool isActiveKaryawan;
  String namaDriver;
  String nikDriver;
  bool isActiveDriver;
  String imageDriver;
  int idLokasiDriver;
  bool isOnlineDriver;
  String namaDivisi;
  String kodeDivisi;
  int lantaiDivisi;
  String tglNotif;

  DataNotifDriver(
      {this.idNotif,
      this.idUser,
      this.notif,
      this.createdAt,
      this.idOrder,
      this.id,
      this.name,
      this.email,
      this.emailVerifiedAt,
      this.password,
      this.rememberToken,
      this.updatedAt,
      this.idRole,
      this.idKaryawan,
      this.idDriver,
      this.fcmToken,
      this.device,
      this.bookingStatus,
      this.idOrderStatus,
      this.idOrderType,
      this.idDriverOrder,
      this.orderCode,
      this.orderDate,
      this.lastUpdatedOn,
      this.jumlahPenumpang,
      this.keperluan,
      this.pickupDate,
      this.pickupLatitude,
      this.pickupLongitude,
      this.pickupLocation,
      this.dropDate,
      this.dropLatitude,
      this.dropLongitude,
      this.dropLocation,
      this.realDistance,
      this.realDuration,
      this.isGanjilGenap,
      this.idRating,
      this.commentRating,
      this.cancelDate,
      this.commentCancel,
      this.startDate,
      this.catatan,
      this.idDivisi,
      this.namaKaryawan,
      this.nikKaryawan,
      this.isActiveKaryawan,
      this.namaDriver,
      this.nikDriver,
      this.isActiveDriver,
      this.imageDriver,
      this.idLokasiDriver,
      this.isOnlineDriver,
      this.namaDivisi,
      this.kodeDivisi,
      this.lantaiDivisi,
      this.tglNotif});

  DataNotifDriver.fromJson(Map<String, dynamic> json) {
    idNotif = json['id_notif'];
    idUser = json['id_user'];
    notif = json['notif'];
    createdAt = json['created_at'];
    idOrder = json['id_order'];
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    password = json['password'];
    rememberToken = json['remember_token'];
    updatedAt = json['updated_at'];
    idRole = json['id_role'];
    idKaryawan = json['id_karyawan'];
    idDriver = json['id_driver'];
    fcmToken = json['fcm_token'];
    device = json['device'];
    bookingStatus = json['booking_status'];
    idOrderStatus = json['id_order_status'];
    idOrderType = json['id_order_type'];
    idDriverOrder = json['id_driver_order'];
    orderCode = json['order_code'];
    orderDate = json['order_date'];
    lastUpdatedOn = json['last_updated_on'];
    jumlahPenumpang = json['jumlah_penumpang'];
    keperluan = json['keperluan'];
    pickupDate = json['pickup_date'];
    pickupLatitude = json['pickup_latitude'];
    pickupLongitude = json['pickup_longitude'];
    pickupLocation = json['pickup_location'];
    dropDate = json['drop_date'];
    dropLatitude = json['drop_latitude'];
    dropLongitude = json['drop_longitude'];
    dropLocation = json['drop_location'];
    realDistance = json['real_distance'];
    realDuration = json['real_duration'];
    isGanjilGenap = json['is_ganjil_genap'];
    idRating = json['id_rating'];
    commentRating = json['comment_rating'];
    cancelDate = json['cancel_date'];
    commentCancel = json['comment_cancel'];
    startDate = json['start_date'];
    catatan = json['catatan'];
    idDivisi = json['id_divisi'];
    namaKaryawan = json['nama_karyawan'];
    nikKaryawan = json['nik_karyawan'];
    isActiveKaryawan = json['is_active_karyawan'];
    namaDriver = json['nama_driver'];
    nikDriver = json['nik_driver'];
    isActiveDriver = json['is_active_driver'];
    imageDriver = json['image_driver'];
    idLokasiDriver = json['id_lokasi_driver'];
    isOnlineDriver = json['is_online_driver'];
    namaDivisi = json['nama_divisi'];
    kodeDivisi = json['kode_divisi'];
    lantaiDivisi = json['lantai_divisi'];
    tglNotif = json['tgl_notif'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_notif'] = this.idNotif;
    data['id_user'] = this.idUser;
    data['notif'] = this.notif;
    data['created_at'] = this.createdAt;
    data['id_order'] = this.idOrder;
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['password'] = this.password;
    data['remember_token'] = this.rememberToken;
    data['updated_at'] = this.updatedAt;
    data['id_role'] = this.idRole;
    data['id_karyawan'] = this.idKaryawan;
    data['id_driver'] = this.idDriver;
    data['fcm_token'] = this.fcmToken;
    data['device'] = this.device;
    data['booking_status'] = this.bookingStatus;
    data['id_order_status'] = this.idOrderStatus;
    data['id_order_type'] = this.idOrderType;
    data['id_driver_order'] = this.idDriverOrder;
    data['order_code'] = this.orderCode;
    data['order_date'] = this.orderDate;
    data['last_updated_on'] = this.lastUpdatedOn;
    data['jumlah_penumpang'] = this.jumlahPenumpang;
    data['keperluan'] = this.keperluan;
    data['pickup_date'] = this.pickupDate;
    data['pickup_latitude'] = this.pickupLatitude;
    data['pickup_longitude'] = this.pickupLongitude;
    data['pickup_location'] = this.pickupLocation;
    data['drop_date'] = this.dropDate;
    data['drop_latitude'] = this.dropLatitude;
    data['drop_longitude'] = this.dropLongitude;
    data['drop_location'] = this.dropLocation;
    data['real_distance'] = this.realDistance;
    data['real_duration'] = this.realDuration;
    data['is_ganjil_genap'] = this.isGanjilGenap;
    data['id_rating'] = this.idRating;
    data['comment_rating'] = this.commentRating;
    data['cancel_date'] = this.cancelDate;
    data['comment_cancel'] = this.commentCancel;
    data['start_date'] = this.startDate;
    data['catatan'] = this.catatan;
    data['id_divisi'] = this.idDivisi;
    data['nama_karyawan'] = this.namaKaryawan;
    data['nik_karyawan'] = this.nikKaryawan;
    data['is_active_karyawan'] = this.isActiveKaryawan;
    data['nama_driver'] = this.namaDriver;
    data['nik_driver'] = this.nikDriver;
    data['is_active_driver'] = this.isActiveDriver;
    data['image_driver'] = this.imageDriver;
    data['id_lokasi_driver'] = this.idLokasiDriver;
    data['is_online_driver'] = this.isOnlineDriver;
    data['nama_divisi'] = this.namaDivisi;
    data['kode_divisi'] = this.kodeDivisi;
    data['lantai_divisi'] = this.lantaiDivisi;
    data['tgl_notif'] = this.tglNotif;
    return data;
  }
}
