class ModelHistoryDriver {
  String result;
  String message;
  List<DataHistoryDriver> data;

  ModelHistoryDriver({this.result, this.message, this.data});

  ModelHistoryDriver.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataHistoryDriver>();
      json['data'].forEach((v) {
        data.add(new DataHistoryDriver.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataHistoryDriver {
  int idOrder;
  int idOrderStatus;
  int idOrderType;
  int idKaryawan;
  int idDriver;
  String orderCode;
  String orderDate;
  String lastUpdatedOn;
  int jumlahPenumpang;
  String keperluan;
  String pickupDate;
  String pickupLatitude;
  String pickupLongitude;
  String pickupLocation;
  String dropDate;
  String dropLatitude;
  String dropLongitude;
  String dropLocation;
  String realDistance;
  String realDuration;
  bool isGanjilGenap;
  int idRating;
  String commentRating;
  String cancelDate;
  String commentCancel;
  String startDate;
  String catatan;
  String namaKaryawan;
  String nikKaryawan;
  String namaDivisi;
  int angkaRating;
  String descRating;
  String namaUnit;
  String nopolUnit;

  DataHistoryDriver(
      {this.idOrder,
      this.idOrderStatus,
      this.idOrderType,
      this.idKaryawan,
      this.idDriver,
      this.orderCode,
      this.orderDate,
      this.lastUpdatedOn,
      this.jumlahPenumpang,
      this.keperluan,
      this.pickupDate,
      this.pickupLatitude,
      this.pickupLongitude,
      this.pickupLocation,
      this.dropDate,
      this.dropLatitude,
      this.dropLongitude,
      this.dropLocation,
      this.realDistance,
      this.realDuration,
      this.isGanjilGenap,
      this.idRating,
      this.commentRating,
      this.cancelDate,
      this.commentCancel,
      this.startDate,
      this.catatan,
      this.namaKaryawan,
      this.nikKaryawan,
      this.namaDivisi,
      this.angkaRating,
      this.descRating,
      this.namaUnit,
      this.nopolUnit});

  DataHistoryDriver.fromJson(Map<String, dynamic> json) {
    idOrder = json['id_order'];
    idOrderStatus = json['id_order_status'];
    idOrderType = json['id_order_type'];
    idKaryawan = json['id_karyawan'];
    idDriver = json['id_driver'];
    orderCode = json['order_code'];
    orderDate = json['order_date'];
    lastUpdatedOn = json['last_updated_on'];
    jumlahPenumpang = json['jumlah_penumpang'];
    keperluan = json['keperluan'];
    pickupDate = json['pickup_date'];
    pickupLatitude = json['pickup_latitude'];
    pickupLongitude = json['pickup_longitude'];
    pickupLocation = json['pickup_location'];
    dropDate = json['drop_date'];
    dropLatitude = json['drop_latitude'];
    dropLongitude = json['drop_longitude'];
    dropLocation = json['drop_location'];
    realDistance = json['real_distance'];
    realDuration = json['real_duration'];
    isGanjilGenap = json['is_ganjil_genap'];
    idRating = json['id_rating'];
    commentRating = json['comment_rating'];
    cancelDate = json['cancel_date'];
    commentCancel = json['comment_cancel'];
    startDate = json['start_date'];
    catatan = json['catatan'];
    namaKaryawan = json['nama_karyawan'];
    nikKaryawan = json['nik_karyawan'];
    namaDivisi = json['nama_divisi'];
    angkaRating = json['angka_rating'];
    descRating = json['desc_rating'];
    namaUnit = json['nama_unit'];
    nopolUnit = json['nopol_unit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_order'] = this.idOrder;
    data['id_order_status'] = this.idOrderStatus;
    data['id_order_type'] = this.idOrderType;
    data['id_karyawan'] = this.idKaryawan;
    data['id_driver'] = this.idDriver;
    data['order_code'] = this.orderCode;
    data['order_date'] = this.orderDate;
    data['last_updated_on'] = this.lastUpdatedOn;
    data['jumlah_penumpang'] = this.jumlahPenumpang;
    data['keperluan'] = this.keperluan;
    data['pickup_date'] = this.pickupDate;
    data['pickup_latitude'] = this.pickupLatitude;
    data['pickup_longitude'] = this.pickupLongitude;
    data['pickup_location'] = this.pickupLocation;
    data['drop_date'] = this.dropDate;
    data['drop_latitude'] = this.dropLatitude;
    data['drop_longitude'] = this.dropLongitude;
    data['drop_location'] = this.dropLocation;
    data['real_distance'] = this.realDistance;
    data['real_duration'] = this.realDuration;
    data['is_ganjil_genap'] = this.isGanjilGenap;
    data['id_rating'] = this.idRating;
    data['comment_rating'] = this.commentRating;
    data['cancel_date'] = this.cancelDate;
    data['comment_cancel'] = this.commentCancel;
    data['start_date'] = this.startDate;
    data['catatan'] = this.catatan;
    data['nama_karyawan'] = this.namaKaryawan;
    data['nik_karyawan'] = this.nikKaryawan;
    data['nama_divisi'] = this.namaDivisi;
    data['angka_rating'] = this.angkaRating;
    data['desc_rating'] = this.descRating;
    data['nama_unit'] = this.namaUnit;
    data['nopol_unit'] = this.nopolUnit;
    return data;
  }
}
