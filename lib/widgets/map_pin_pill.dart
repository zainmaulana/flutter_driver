import 'package:vmsdriver/models/pin_pill_info.dart';
import 'package:flutter/material.dart';

class MapPinPillComponent extends StatefulWidget {
  double pinPillPosition;
  PinInformation currentlySelectedPin;

  MapPinPillComponent({this.pinPillPosition, this.currentlySelectedPin});

  @override
  State<StatefulWidget> createState() => MapPinPillComponentState();
}

class MapPinPillComponentState extends State<MapPinPillComponent> {
  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      bottom: widget.pinPillPosition,
      right: 0,
      left: 0,
      duration: Duration(milliseconds: 200),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          margin: EdgeInsets.all(20),
          height: 150,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(50)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    blurRadius: 20,
                    offset: Offset.zero,
                    color: Colors.grey.withOpacity(0.5))
              ]),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.only(left: 10, right: 15),
                child: ClipOval(
                    child: Image.network(widget.currentlySelectedPin.avatarPath,
                        fit: BoxFit.cover)),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          widget.currentlySelectedPin.name ?? "",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      setStateUnit(),
                      Flexible(
                        child: Text(
                          widget.currentlySelectedPin.locationName ?? "",
                          style: TextStyle(
                              color: widget.currentlySelectedPin.labelColor),
                        ),
                      ),
                      // Text(
                      //   'Latitude: ${widget.currentlySelectedPin.location.latitude.toString()}',
                      //   style: TextStyle(fontSize: 12, color: Colors.grey),
                      // ),
                      // Text(
                      //   'Longitude: ${widget.currentlySelectedPin.location.longitude.toString()}',
                      //   style: TextStyle(fontSize: 12, color: Colors.grey),
                      // ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget setStateUnit() {
    if (widget.currentlySelectedPin.unitKendaraan != null &&
        widget.currentlySelectedPin.nopolUnit != null) {
      return Flexible(
        child: Text(
          widget.currentlySelectedPin.unitKendaraan +
                  " - " +
                  widget.currentlySelectedPin.nopolUnit ??
              "",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w500,
          ),
        ),
      );
    }
    return Container();
  }
}
