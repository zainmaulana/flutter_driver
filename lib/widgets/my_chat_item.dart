import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmsdriver/models/model_driverchat.dart';
import 'package:vmsdriver/screens/chatroom_screen.dart';

class ChatItem extends StatelessWidget {
  DataChatDriver dataChatDriver;
  ChatItem({Key key, this.dataChatDriver}) : super(key: key);

  Future<void> setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setInt("idorder", dataChatDriver.idOrder);
    print("ID Order : " + dataChatDriver.idOrder.toString());
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setPref();
        Toast.show(dataChatDriver.orderCode, context);
        Navigator.of(context)
            .pushNamed(ChatRoom.tag, arguments: dataChatDriver);
      },
      child: Card(
        elevation: 7,
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: 30,
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(dataChatDriver.orderCode,
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.black)),
                ),
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                        child: Row(
                          children: [
                            Icon(
                              Icons.person,
                              size: 30,
                              color: Colors.green,
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  dataChatDriver.namaKaryawan,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                        child: Row(
                          children: [
                            Icon(
                              Icons.drive_eta,
                              size: 30,
                              color: Colors.black,
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  dataChatDriver.nopolUnit,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                        child: Row(
                          children: [
                            Icon(
                              Icons.airport_shuttle,
                              size: 30,
                              color: Colors.red[700],
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  dataChatDriver.namaUnit,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                    child: Row(
                      children: [
                        Icon(
                          Icons.chat_outlined,
                          size: 30,
                          color: Colors.blue[900],
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(
                              dataChatDriver.lastChat,
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    dataChatDriver.orderDate,
                    textAlign: TextAlign.end,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
