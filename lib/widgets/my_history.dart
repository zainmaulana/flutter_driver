import 'package:vmsdriver/models/model_driverhistory.dart';
import 'package:vmsdriver/widgets/my_history_item.dart';
import 'package:flutter/material.dart';

class History extends StatelessWidget {
  List<DataHistoryDriver> dataHistoryDriver;
  History({Key key, this.dataHistoryDriver}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataHistoryDriver?.length ?? 0,
      itemBuilder: (context, index) => HistoryItem(
        dataHistoryDriver: dataHistoryDriver[index],
      ),
    );
  }
}
