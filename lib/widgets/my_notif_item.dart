import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmsdriver/models/model_drivernotification.dart';
import 'package:vmsdriver/screens/detailnotif.dart';

class NotifItem extends StatelessWidget {
  DataNotifDriver dataNotifDriver;
  NotifItem({Key key, this.dataNotifDriver}) : super(key: key);

  Future<void> setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (dataNotifDriver.idDriver == dataNotifDriver.idDriverOrder) {
      pref.setInt("iddriver", dataNotifDriver.idUser);
      print("ID Driver : " + dataNotifDriver.idUser.toString());
    } else {
      pref.setInt("iddriver", 0);
      print("ID Driver : 0");
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setPref();
        Toast.show(dataNotifDriver.orderCode, context);
        Navigator.of(context)
            .pushNamed(DetailNotif.tag, arguments: dataNotifDriver);
      },
      child: Card(
        elevation: 7,
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: 30,
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(dataNotifDriver?.orderCode ?? "Kosong",
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.black)),
                ),
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 4),
              child: Row(
                children: [
                  Icon(
                    Icons.notifications,
                    size: 30,
                    color: Colors.blue[900],
                  ),
                  Flexible(
                    child: Text(
                      dataNotifDriver.notif,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    "Tanggal Notif : " + dataNotifDriver?.tglNotif ?? "Kosong",
                    textAlign: TextAlign.end,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
