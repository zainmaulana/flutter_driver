import 'package:flutter/material.dart';
import 'package:vmsdriver/models/model_driverchat.dart';
import 'package:vmsdriver/widgets/my_chat_item.dart';

class ChatDriver extends StatelessWidget {
  List<DataChatDriver> dataChatDriver;
  ChatDriver({Key key, this.dataChatDriver = null}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataChatDriver?.length ?? 0,
      itemBuilder: (context, index) => ChatItem(
        dataChatDriver: dataChatDriver[index],
      ),
    );
  }
}
