import 'package:flutter/material.dart';
import 'package:vmsdriver/models/model_drivernotification.dart';
import 'package:vmsdriver/widgets/my_notif_item.dart';

class NotifDriver extends StatelessWidget {
  List<DataNotifDriver> dataNotifDriver;
  NotifDriver({Key key, this.dataNotifDriver = null}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataNotifDriver?.length ?? 0,
      itemBuilder: (context, index) => NotifItem(
        dataNotifDriver: dataNotifDriver[index],
      ),
    );
  }
}
