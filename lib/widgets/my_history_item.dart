import 'package:vmsdriver/models/model_driverhistory.dart';
import 'package:vmsdriver/screens/detailhistory.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class HistoryItem extends StatelessWidget {
  DataHistoryDriver dataHistoryDriver;
  HistoryItem({Key key, this.dataHistoryDriver}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Toast.show(dataHistoryDriver.orderCode, context);
        Navigator.of(context)
            .pushNamed(DetailHistory.tag, arguments: dataHistoryDriver);
      },
      child: Card(
        elevation: 7,
        child: Column(
          children: [
            Container(
              color: Colors.blue[900],
              height: 30,
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(dataHistoryDriver?.orderCode ?? "Kosong",
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.white)),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 4),
              child: Row(
                children: [
                  Icon(
                    Icons.place,
                    size: 30,
                    color: Colors.green,
                  ),
                  Flexible(
                    child: Text(
                      dataHistoryDriver.dropLocation,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    "Tanggal Order : " + dataHistoryDriver?.orderDate ??
                        "Kosong",
                    textAlign: TextAlign.end,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
